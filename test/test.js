var File = require('vinyl');
var bufferToStream = require('simple-bufferstream');
var removeCss = require('../index.js');
var fs = require('fs');
var expect = require('chai').expect;


describe('gulp-remove-unused-css', function () {
    it('should process html files', function (done) {
        runTest({
            folder: 'html'
        }, done);
    });

    it('should process js files', function (done) {
        runTest({
            folder: 'js'
        }, done);
    });

    it('should process php files', function (done) {
        runTest({
            folder: 'php'
        }, done);
    });

    it('should process mixed files', function (done) {
        runTest({
            folder: 'mixed'
        }, done);
    });

    it('should handle class enumeration', function (done) {
        runTest({
            folder: 'classes_enumeration'
        }, done);
    });


    it('should ignore classes cascade', function (done) {
        runTest({
            folder: 'cascade'
        }, done);
    });

    it('should ignore classes cascade with spaces', function (done) {
        runTest({
            folder: 'cascade_with_spaces'
        }, done);
    });

    it('should handle pseudo classes', function (done) {
        runTest({
            folder: 'pseudo_classes'
        }, done);
    });

    function runTest(options, done) {
        var contents = fs.readFileSync('test/data/' + options.folder + '/given.css', 'utf8');
        var expected = fs.readFileSync('test/data/' + options.folder + '/expected.css', 'utf8');

        removeCss({
            path: ['test/data/' + options.folder + '/**/*.html',
                'test/data/' + options.folder + '/**/*.php',
                'test/data/' + options.folder + '/**/*.js'
            ]
        })
            .on('error', done)
            .on('data', function (file) {
                expect(String(file.contents)).to.equal(expected);
                done();
            })
            .end(new File({contents: bufferToStream(contents)}));
    }
});
