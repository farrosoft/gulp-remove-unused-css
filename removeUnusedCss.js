var fs = require("fs"),
    vow = require('vow'),
    glob = require("multi-glob").glob,
    css = require('css');

function getAllUsedClasses(path, usedClasses) {
    var deferred = vow.defer();

    glob(path, function (er, files) {
        var deferredMap = vow.defer();
        var size = files.length;
        var done = 0;

        if (size === 0)
            deferred.resolve('ok');

        files.map(function (file, i) {
            return getClassesFromFile(file, usedClasses, deferredMap).then(function () {
                done++;
                if (done == size)
                    deferred.resolve('ok');
            });
        });
    });

    return deferred.promise();
}

function getClassesFromFile(file, usedClasses, deferredMap) {
    fs.readFile(file, {encoding: 'utf-8'}, function (err, data) {
        if (!err) {
            getAllClassesFromClassDeclaration(data, usedClasses);
            getAllClassesForJs(data, usedClasses);
        } else {
            console.log(err);
        }
        deferredMap.resolve('ok');
    });
    return deferredMap.promise();
}

function getAllClassesFromClassDeclaration(data, usedClasses) {
    var re = /class=["|'](.*?)["|']/g;

    var match = re.exec(data);
    while (match != null) {
        var classes = match[1];
        classes.split(" ").map(function (item, i) {
            usedClasses[item] = 1;
        });
        match = re.exec(data);
    }
}

function getAllClassesForJs(data, usedClasses) {
    var re = /[addClass|toggleClass]\(["|'](.*?)["|']\)/g;

    var match = re.exec(data);
    while (match != null) {
        var foundClass = match[1];
        usedClasses[foundClass] = 1;
        match = re.exec(data);
    }
}

function removeUnusedClassesFromCss(data, usedClasses) {
    var obj = css.parse(data);
    var rules = obj['stylesheet']['rules'];
    var newRules = [];

    addRulesIfUsed(rules, newRules, usedClasses);

    obj['stylesheet']['rules'] = newRules;
    result = css.stringify(obj, {'indent': '    '});
    return result;
}

function addRulesIfUsed(rules, newRules, usedClasses) {
    for (var i = 0; i < rules.length; i++) {
        var item = rules[i];

        if (item['type'] === 'rule') {
            addRuleIfUsed(item, newRules, usedClasses);
        } else if (item['type'] === 'media') {
            var mediaRules = item['rules'];
            var newMediaRules = [];
            addRulesIfUsed(mediaRules, newMediaRules, usedClasses);

            item['rules'] = newMediaRules;
            newRules.push(item);
        } else {
            newRules.push(item);
        }
    }
}

function addRuleIfUsed(item, newRules, usedClasses) {

    var selectors = item['selectors'];
    var selectorsToAdd = [];

    for (var j = 0; j < selectors.length; j++) {
        var selector = selectors[j];

        if (!isSingleClass(selectors[j])) {
            newRules.push(item);
            return;
        }

        var selectorClassName = getClassName(selector);

        if (isUsed(usedClasses, selectorClassName)) {
            selectorsToAdd.push(selector);
        }
    }

    if (selectorsToAdd.length !== 0) {
        item['selectors'] = selectorsToAdd;
        newRules.push(item);
    }
}

function isSingleClass(className) {
    var startsAsClass = className.indexOf('.', 0) === 0;
    if (!startsAsClass)
        return false;

    var hasNoCascade = className.split('.').length === 2;
    if (!hasNoCascade)
        return false;

    var hasNoCascadeWithSpace = className.split(' ').length === 1;
    if (!hasNoCascadeWithSpace)
        return false;

    return true;
}

function getClassName(selector) {
    var isPseudoClass = selector.split(':');
    if (isPseudoClass.length === 2)
        selector = selector.split(':')[0];

    return selector.substring(1)
}

function isHolster(className) {
    return className.indexOf('h--', 0) === 0;
}

function isBlock(className) {
    return className.indexOf('b-', 0) === 0;
}

function isUsed(usedClasses, className) {
    return !!usedClasses[className];
}

function hasMultipleClasses(selectors) {
    return selectors.length > 1;
}

module.exports.getAllUsedClasses = getAllUsedClasses;
module.exports.removeUnusedClassesFromCss = removeUnusedClassesFromCss;
