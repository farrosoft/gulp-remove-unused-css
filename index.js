var removeUnusedCss = require('./removeUnusedCss');

var Transform = require('readable-stream/transform');
var tryit = require('tryit');
var BufferStreams = require('bufferstreams');

module.exports = function gulpHtmlmin(options) {
    return new Transform({
        objectMode: true,
        transform: function htmlminTransform(file, enc, cb) {
            if (file.isNull()) {
                cb(null, file);
                return;
            }

            function minifyHtml(buf, done) {
                var result;
                var data = String(buf);

                tryit(function () {
                    var usedClasses = {};
                    removeUnusedCss.getAllUsedClasses(options.path, usedClasses).then(function () {
                        var resultAsString = removeUnusedCss.removeUnusedClassesFromCss(data, usedClasses);
                        result = new Buffer(resultAsString, "utf-8");
                        done(null, result);
                    }).done();
                }, function (err) {
                    if (err) {
                        options = objectAssign({}, options, {fileName: file.path});
                        done(new gutil.PluginError('gulp-htmlmin', err, options));
                    }
                });
            }

            var self = this;

            if (file.isStream()) {
                file.contents.pipe(new BufferStreams(function (none, buf, done) {
                    minifyHtml(buf, function (err, contents) {
                        if (err) {
                            self.emit('error', err);
                            done(err);
                        } else {

                            done(null, contents);
                            file.contents = contents;
                            self.push(file);
                        }
                        cb();
                    });
                }));
                return;
            }
        }
    });
};